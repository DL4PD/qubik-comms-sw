/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ANTENNA_H_
#define ANTENNA_H_

#include "main.h"

#define ANT_DEPLOY_TEST_TIME		200		//!< Time to test antenna deploy mechanism
#define ANT_DEPLOY_TEST_CURRENT		500		//!< Lower limit of antenna deploy current
#define ANT_DEPLOY_TIME				120		//!< Max time in seconds to keep antenna deploy powered
#define ANTENNA_POST_DEPLOY_DELAY 	1000	//!< Time in ms to keep deployment active after deployment detection


/**
 * @brief Antenna deploy commands
 */
typedef enum {
	ANT_DEPLOY_ON = SET,	//!< ANT_DEPLOY_ON
	ANT_DEPLOY_OFF = RESET	//!< ANT_DEPLOY_OFF
} ant_deploy_power_t;

/**
 * @brief Antenna deploy states
 */
typedef enum {
	ANT_STOWED,				//!< Antenna is stowed
	ANT_DEPLOYED			//!< Antenna is deployed
} ant_deploy_status_t;

/**
 * @brief Antenna deploy test states
 */
typedef enum {
	ANT_DEPLOY_TEST_OK,      	//!< Deploy test was successful
	ANT_DEPLOY_TEST_DEGRADED,	//!< Deploy system is degraded.
	ANT_DEPLOY_TEST_FAIL,     	//!< Deploy test has failed
	ANT_DEPLOY_TEST_PENDING		//!< Deploy test pending
} ant_deploy_test_status_t;

/**
 * @brief Antenna deploy status
 */
struct ant_status {
	ant_deploy_test_status_t ant_deploy_test;	//!< Antenna deploy test result
	ant_deploy_status_t ant_deploy_status;		//!< Antenna deploy status
	uint8_t antenna_deploy_time;				//!< Antenna deploy elapsed time. Time in seconds it took for the antenna to deploy
};

ant_deploy_test_status_t
antenna_deploy_test();
ant_deploy_status_t
antenna_deploy_status();
uint8_t
antenna_deploy();

#endif /* ANTENNA_H_ */
