/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPI_UTILS_H_
#define SPI_UTILS_H_

#include <stdint.h>
#include <Drivers/AX5043/Inc/ax5043.h>

#define SPI_TIMEOUT                     1000

/**
 * Enumeration of all the available SPI devices
 */
typedef enum {
	SPI_DEV_AX5043 = 0,           //!< AX5043 RF chip
	SPI_DEV_NUM                   //!< Total number of SPI devices
} spi_dev_t;


int
ax5043_drv_dev_select(bool enable);

int
ax5043_drv_spi_read(uint8_t *rx, uint8_t *tx, uint32_t len);

int
ax5043_drv_spi_write(uint8_t *rx, uint8_t *tx, uint32_t len);

#endif /* SPI_UTILS_H_ */
