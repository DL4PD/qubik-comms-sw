/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CONF_H_
#define CONF_H_

/**
 * @file conf.h
 * General configuration file
 */
#define PCB_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))

/*
 *  Define here your PCB version according to the tag of
 * https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw
 */
//#define PQ_PCB_VERSION                  PCB_VERSION(0,9,5)

#ifndef PQ_PCB_VERSION
#error "The PCB version is undefined. Please use the PCB_VERSION(a,b,c) to define it. Check your version at https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw"
#endif

/**
 * Enable/disable the tests performed from the default task.
 * @note set it always to 0 for the FM firmware
 */
#define QUBIK_ENABLE_TEST               0

/**
 * Serial number of the Qubik satellite
 */
#define QUBIK_SN                        0

#if QUBIK_SN==0
#define QUBIK_SAT_ID                    0x32A109F7
#define QUBIK_NAME                      "QUBIK-0"
#define QUBIK_TX_FREQ_HZ                433200000
#define QUBIK_RX_FREQ_HZ                433200000
#else
#define QUBIK_SAT_ID                    0xCF341F0A
#define QUBIK_NAME                      "QUBIK-1"
#define QUBIK_TX_FREQ_HZ                433200000
#define QUBIK_RX_FREQ_HZ                433200000
#endif
#define QUBIK_XTAL_FREQ_HZ              26e6
#define QUBIK_FLASH_MAGIC_VAL           0x6E67DF61
#define QUBIK_STORAGE_FLASH_ADDR        0x0807F800



/**
 * Ramp up/Ramp down period of the power amplifier in microseconds
 */
#define PWRAMP_RAMP_PERIOD_US           200

#define AX5043_RF_SWITCH_ENABLE         ANTSEL_OUTPUT_1
#define AX5043_RF_SWITCH_DISABLE        ANTSEL_OUTPUT_0

/******************************************************************************
 ****************************** Task delays ***********************************
 *****************************************************************************/

#define WDG_TASK_DELAY_MS               200

/******************************************************************************
 ****************************** OSDLP Params **********************************
 *****************************************************************************/

#define OSDLP_TC_ITEMS_PER_QUEUE        20
#define OSDLP_TM_ITEMS_PER_QUEUE        20
#define OSDLP_MAX_TC_FRAME_SIZE         64
#define OSDLP_TM_FRAME_SIZE             64
#define OSDLP_TC_VCS                    5
#define OSDLP_TM_VCS                    5
#define OSDLP_SCID                      20
#define OSDLP_MAX_TC_PACKET_LENGTH      200 + 2
#define OSDLP_MAX_TM_PACKET_LENGTH      200 + 2

/******************************************************************************
 ****************************** VC IDs **********************************
 *****************************************************************************/

#define VCID_MANAGEMENT                0
#define VCID_TM                        1
#define VCID_EXT_TM                    2
#define VCID_EXPERIMENT                3
#define VCID_CONTACT                   4

#endif /* CONF_H_ */
