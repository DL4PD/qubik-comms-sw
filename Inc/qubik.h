/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QUBIK_H_
#define QUBIK_H_

#include "ft_storage.h"
#include "radio.h"
#include "max17261_driver.h"
#include "power.h"
#include "antenna.h"

struct qubik_settings {
	uint32_t init;
	uint32_t sat_id;
	uint8_t first_deploy;
	uint32_t tx_freq;
	uint32_t rx_freq;

	uint16_t dropped_frames;
};

/**
 * @brief Hardware status bits
 */
struct qubik_hw_status {
	uint8_t power_save: 1;		//!< Power save mode 1: True
	uint8_t power_mon_fail: 1;	//!< Power monitor failed 1: True
};

struct qubik {
	struct radio hradio;
	struct max17261_conf hmax17261;
	struct power_status power;
	struct ant_status antenna;
	struct ft_storage ft_persist_mem;
	struct qubik_settings settings;
	uint32_t uptime_secs;
	struct qubik_hw_status status;
};

int
qubik_init(struct qubik *q);

int
qubik_read_settings(struct qubik *q);

int
qubik_write_settings(struct qubik *q);

#endif /* QUBIK_H_ */
