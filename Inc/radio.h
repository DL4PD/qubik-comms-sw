/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RADIO_H_
#define RADIO_H_

#include <stdlib.h>
#include <stdint.h>
#include <Drivers/AX5043/Inc/ax5043.h>

/**
 * The maximum RX frame size
 */
#define MAX_RX_FRAME_LEN 1024

/**
 * The maximum number of RX frames that can wait at the RX queue to be processed
 * by the OSDLP logic
 */
#define MAX_RX_FRAMES    4

/**
 * The maximum TX frame size
 */
#define MAX_TX_FRAME_LEN 512

/**
 * The maximum number of outstanding frames that wait to be transmitted through
 * the AX5043
 */
#define MAX_TX_FRAMES    16

typedef enum {
	RADIO_STATE_INIT,
	RADIO_STATE_IDLE,
	RADIO_STATE_POWER_DOWN,
	RADIO_STATE_DEEP_SLEEP,
	RADIO_STATE_RX,
	RADIO_STATE_TX
} radio_state_t;

struct radio {
	radio_state_t state;
	struct ax5043_conf hax5043;
};

struct rx_frame {
	uint32_t len;
	uint8_t pdu[MAX_RX_FRAME_LEN];
};

struct tx_frame {
	uint16_t len;
	uint32_t timeout_ms;
	uint8_t pdu[MAX_TX_FRAME_LEN];
};


int
radio_init(struct radio *hradio);

void
radio_frame_received(const uint8_t *pdu, size_t len);

void
radio_tx_task();

#endif /* RADIO_H_ */
