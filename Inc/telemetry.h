/*
 *
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.h
 *
 *  @date Jul 21, 2020
 *  @brief Telemetry frame definitions
 */

#ifndef INC_TELEMETRY_H_
#define INC_TELEMETRY_H_

#include "qubik.h"
#include <stdint.h>

size_t
telemetry_assemble_regular_frame(struct qubik *q, uint8_t *buffer);
size_t
telemetry_assemble_extended_frame(struct qubik *q, uint8_t *buffer);
#endif /* INC_TELEMETRY_H_ */
