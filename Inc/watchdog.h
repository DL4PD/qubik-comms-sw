/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

struct watchdog {
	uint8_t               subsystems_num;
	uint32_t              subsystems;
	uint32_t              subsystems_mask;
	uint8_t               registered;
	osMutexId             mtx;
	IWDG_HandleTypeDef    *hiwdg;
};

int
watchdog_init(struct watchdog *w, IWDG_HandleTypeDef *hiwdg,
              osMutexId mtx, uint8_t n);

int
watchdog_register(struct watchdog *w, uint8_t *id);

int
watchdog_reset(struct watchdog *w);

int
watchdog_reset_subsystem(struct watchdog *w, uint8_t id);


#endif /* WATCHDOG_H_ */
