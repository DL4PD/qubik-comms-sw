# Qubik: An open source 5x5x5 pico-satellite

Qubik COMMS utilizes the STM32L476 MCU and is equipped with the AX5043 transceiver
operating at the UHF amateur band. 

![Qubik Logo](docs/assets/qubik-logo-1.png)
![Qubik assembly](docs/assets/qubik_clay02.png)

## Development Guide
Qubik COMMS software in independent of any kind of development tool. 
You can use the development environment of your choice. 
In addition the project contains a [Makefile](Makefile) that can be used to
build the firmware automatically.

However we recommend the usage of the TrueSTUDIO or STM32CubeIDE for the
development, debugging and experimentation.
These IDEs have been optimized for the STM32 MCUs and provide helpful 
graphical debugging tools.
More information regarding the import process of the project into these tools 
can be found at [Import into the TrueSTUDIO or STM32CubeIDE](#import-into-the-truestudio-or-stm32cubeide) section.

### Requirements
* GNU Make
* cross-arm-none-eabi-gcc (>= 5.0)
* STM32 CubeMX (for modifying peripherals and FreeRTOS parameters)

### Dependencies
The Qubik COMMS codebase depends on the [AX5043 Driver](https://gitlab.com/librespacefoundation/ax5043-driver) which is shipped as git submodule within the project.

### Define the PCB version
The Qubik COMMS software can operate on a different set of PCB versions that 
can be retrieved from the 
[PQ9ISH COMMS VU hw](https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw)
repository.
To compile the firmware for the prober PCB version, developers should define
the `PQ_PCB_VERSION`using the `PCB_VERSION(a,b,c)` located in the `Inc/conf.h`
header file. Failed to do so, it will generate a compile time error.
For example:

```c
#define PCB_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))

/*
 *  Define here your PCB version according to the tag of
 * https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw
 */
#define PQ_PCB_VERSION PCB_VERSION(0,9,5)

#ifndef PQ_PCB_VERSION
#error "The PCB version is undefined. Please use the PCB_VERSION(a,b,c) to define it. Check your version at https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw"
#endif
```

**Note:** Please do not stage the modified `Inc/conf.h` into your commits. 
{: .note}

### Coding Style
For the C code, we use a slightly modified version of the 
**Linux Kernel** style. Use `astyle` and the options file `.astylerc` to
adapt to the styling.

At the root directory of the project there is the `astyle` options 
file `.astylerc` containing the proper configuration.
Developers can import this configuration to their favorite editor. 
In addition the `hooks/pre-commit` file contains a Git hook, 
that can be used to perform before every commit, code style formatting
with `astyle` and the `.astylerc` parameters.
To enable this hook developers should copy the hook at their `.git/hooks` 
directory. 
Failing to comply with the coding style described by the `.astylerc` 
will result to failure of the automated tests running on our CI services. 
So make sure that you either import on your editor the coding style rules 
or use the `pre-commit` Git hook.

### Import into the TrueSTUDIO or STM32CubeIDE
- Download TrueSTUDIO or STM32CubeIDE
- Open the `qubik-comms-sw` directory and delete any possible instances of project configuration files (e.g `.project` or `.cproject` and such)
- Launch CubeMX and open the `.ioc` file from the `qubik-comms-sw` directory.
- Go to the Project Properties tab and select either `TrueStudio` or `STM32CubeIDE` on the toolchain drop down list, depending on the IDE of your choice.
- Generate the code

Now some project configuration files have been generated, which are the ones associating the project with TrueSTUDIO or STM32CubeIDE respectively.

- Open your IDE
- Import a project, and choose "Existing Project into workspace", then select the `qubik-comms-sw` directory and proceed with the import
- Do not use the auto-generated linker script. Use the [STM32L476RGTx_FLASH.ld](STM32L476RGTx_FLASH.ld)
that is shipped with the project. To do so, right click the project at the Project Explorer tab and open `Properties`, then expand `C/C++ Build`. At `C Linker` > `General` you will find the necessary field

One last step in order to build correctly:

- Right click the project at the Project Explorer tab and open `Properties`, then expand `C/C++ Build` tab
- Select `Settings` and go to `C Compiler` > `Directories`, then click the `+` button and add `'../Drivers/AX5043/Inc'`

## Documentation
[Doxygen Page](https://librespacefoundation.gitlab.io/qubik/qubik-comms-sw/)

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![Qubik Logo](docs/assets/qubik-logo-1-66x66.png)
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png) 
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).