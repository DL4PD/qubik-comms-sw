/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm.h"
#include "osdlp.h"
#include "osdlp_queue_handle.h"
#include "watchdog.h"
#include "error.h"

extern osMutexId osdlp_tx_mtxHandle;
extern osMutexId osdlp_rx_mtxHandle;
extern struct watchdog hwdg;

int
transmit_tm(uint8_t *pkt, uint16_t length, uint8_t vcid)
{
	struct tm_transfer_frame *tm;
	struct tc_transfer_frame *tc;
	int ret = 0;
	if (osMutexWait(osdlp_tx_mtxHandle, 200) == osOK) {
		ret = tm_get_tx_config(&tm, vcid);
		if (ret)
			return -MTX_TIMEOUT_ERROR;
		ret = osdlp_tc_get_rx_config(&tc, vcid);
		if (ret)
			return ret;
		osdlp_prepare_clcw(tc, tm->ocf);
		ret = osdlp_tm_transmit(tm, pkt, length);
		osMutexRelease(osdlp_tx_mtxHandle);
		return ret;
	} else {
		return -OSDLP_MTX_LOCK;
	}
}

int
receive_tc(uint8_t *pkt, uint16_t *length, uint8_t vcid)
{
	int ret = 0;
	if (osMutexWait(osdlp_rx_mtxHandle, 200) == osOK) {
		if (rx_queues[vcid].capacity > 0) {
			ret = dequeue(&rx_queues[vcid], pkt, length);
		} else {
			ret = -OSDLP_QUEUE_EMPTY;
		}
		osMutexRelease(osdlp_rx_mtxHandle);
	} else {
		return -OSDLP_MTX_LOCK;
	}
	return ret;
}

int
fsm_task()
{
	uint8_t wdgid = 0;
	watchdog_register(&hwdg, &wdgid);
	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(100);
	}
}
