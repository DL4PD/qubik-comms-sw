/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "power.h"
#include "qubik.h"
#include "cmsis_os.h"
#include "error.h"
#include "watchdog.h"
#include "telemetry.h"

extern struct qubik hqubik;
extern ADC_HandleTypeDef hadc1;
extern osMutexId pwr_mtxHandle;
extern struct watchdog hwdg;

/**
 * @brief Update power status structure from max17261 values
 * @param power_status
 * @return 0 on success or negative error code
 */
int
update_power_status(struct power_status *power_status)
{
	int ret = osMutexWait(pwr_mtxHandle, 1000);
	if (ret != osOK) {
		return -MTX_TIMEOUT_ERROR;
	}
	// Get voltage from alternate source
	ret = HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	ret = HAL_ADC_Start(&hadc1);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	// PA PDET
	ret = HAL_ADC_PollForConversion(&hadc1, 100);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		power_status->pa_output_power = HAL_ADC_GetValue(&hadc1) *
		                                ADC_VOLTAGE_COEFFICIENT;
	}

	// Auxiliary voltage monitor
	ret = HAL_ADC_PollForConversion(&hadc1, 100);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		power_status->voltage_alt = HAL_ADC_GetValue(
		                                    &hadc1) * ADC_VBAT_VOLTAGE_COEFFICIENT;
	}

	// MCU Temperature
	ret = HAL_ADC_PollForConversion(&hadc1, 100);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		power_status->temperature_mcu = __LL_ADC_CALC_TEMPERATURE(
		                                        3300, HAL_ADC_GetValue(&hadc1), LL_ADC_RESOLUTION_12B);
//		power_status->temperature_mcu = HAL_ADC_GetValue(&hadc1);
	}

	// RTC Battery voltage
	ret = HAL_ADC_PollForConversion(&hadc1, 100);
	if (ret) {
		osMutexRelease(pwr_mtxHandle);
		return -HAL_to_qubik_error(ret);
	}

	if ((HAL_ADC_GetState(&hadc1) & HAL_ADC_STATE_REG_EOC) ==
	    HAL_ADC_STATE_REG_EOC) {
		power_status->vbat = HAL_ADC_GetValue(&hadc1) *
		                     ADC_VOLTAGE_COEFFICIENT * 3;
	}
	HAL_ADC_Stop(&hadc1);

	// Get power values from MAX17261
	if (max17261_init(&hqubik.hmax17261) == 0) {
		power_status->voltage = max17261_get_voltage(&hqubik.hmax17261);
		power_status->voltage_avg = max17261_get_average_voltage(
		                                    &hqubik.hmax17261);
		power_status->current = max17261_get_current(&hqubik.hmax17261);
		power_status->current_avg = max17261_get_average_current(
		                                    &hqubik.hmax17261);
		power_status->SOC = max17261_get_SOC(&hqubik.hmax17261);
		power_status->temperature = max17261_get_temperature(&hqubik.hmax17261);
		power_status->temperature_die = max17261_get_die_temperature(
		                                        &hqubik.hmax17261);
		power_status->temperature_avg = max17261_get_average_temperature(
		                                        &hqubik.hmax17261);
		power_status->TTE = max17261_get_TTE(&hqubik.hmax17261);

		max17261_get_minmax_voltage(&hqubik.hmax17261,
		                            &power_status->voltage_min,
		                            &power_status->voltage_max);
		max17261_get_minmax_current(&hqubik.hmax17261,
		                            &power_status->current_min,
		                            &power_status->current_max);
	}
	osMutexRelease(pwr_mtxHandle);
	return NO_ERROR;
}

void
power_task()
{
	uint8_t wdgid = 0;
	watchdog_register(&hwdg, &wdgid);
	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		hqubik.status.power_mon_fail = (update_power_status(&hqubik.power)
		                                != NO_ERROR);
		osDelay(1000);
	}
}

