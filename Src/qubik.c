/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "qubik.h"
#include "error.h"
#include "conf.h"
#include "cmsis_os.h"

static void
default_settings(struct qubik_settings *s)
{
	s->init = QUBIK_FLASH_MAGIC_VAL;
	s->first_deploy = 1;
	s->sat_id = QUBIK_SAT_ID;
	s->rx_freq = QUBIK_RX_FREQ_HZ;
	s->tx_freq = QUBIK_TX_FREQ_HZ;
}

int
qubik_init(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	memset(q, 0, sizeof(struct qubik));
	/* Initialize the fault tolerant storage engine */
	int ret = ft_storage_init(&q->ft_persist_mem,
	                          sizeof(struct qubik_settings), QUBIK_STORAGE_FLASH_ADDR);
	if (ret) {
		return ret;
	}
	ret = qubik_read_settings(q);
	if (ret) {
		return ret;
	}

	/* Initialize radio related structures */
	ret = radio_init(&q->hradio);
	if (ret) {
		return ret;
	}

	/* Setup MAX17261 */
	q->hmax17261.delay_ms = osDelay;
	q->hmax17261.read = max17261_read_word;
	q->hmax17261.write = max17261_write_word;
	q->hmax17261.write_verify = max17261_write_verify;
	q->hmax17261.DesignCap = BATTERY_CAPACITY;
	q->hmax17261.IchgTerm = BATTERY_CRG_TERM_I;
	q->hmax17261.VEmpty = (BATTERY_V_EMPTY << 7) | (BATTERY_V_Recovery & 0x7F);
	max17261_init(&q->hmax17261);

	/* FIXME: Set the antenna status? */
	return NO_ERROR;
}

int
qubik_read_settings(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	int ret = ft_storage_read(&q->ft_persist_mem, &q->settings);
	/* In case the read failed, restore the defaults and write back the memory*/
	if (ret) {
		default_settings(&q->settings);
		return qubik_write_settings(q);
	}
	/* Maybe the flash was not initialized */
	if (q->settings.init != QUBIK_FLASH_MAGIC_VAL) {
		default_settings(&q->settings);
		return qubik_write_settings(q);
	}
	return NO_ERROR;
}

int
qubik_write_settings(struct qubik *q)
{
	if (!q) {
		return -INVAL_PARAM;
	}
	return ft_storage_write(&q->ft_persist_mem, &q->settings);
}
