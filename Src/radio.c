/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "radio.h"
#include <string.h>
#include <FreeRTOS.h>
#include "queue.h"
#include "watchdog.h"
#include "error.h"
#include "ax5043_driver.h"
#include "conf.h"
#include "utils.h"
#include "bsp_pq9ish_comms.h"
#include "qubik.h"

extern struct qubik hqubik;
extern struct watchdog hwdg;
extern QueueHandle_t rx_queue;
extern QueueHandle_t tx_queue;

/* NOTE: These objects are quite large to fit inside the stack */
struct tx_frame tx_msg;
struct rx_frame rx_msg;

int
radio_init(struct radio *hradio)
{
	int ret;
	if (!hradio) {
		return -INVAL_PARAM;
	}

	struct xtal xtal = {
		.freq = QUBIK_XTAL_FREQ_HZ,
		.type = TCXO,
		.capacitance = 3
	};
	ret = ax5043_prepare(&hradio->hax5043, &xtal, VCO_INTERNAL,
	                     ax5043_drv_dev_select,
	                     ax5043_drv_spi_read,
	                     ax5043_drv_spi_write,
	                     usleep,
	                     millis,
	                     NULL,
	                     radio_frame_received);
	if (ret) {
		return ret;
	}
	ret = ax5043_init(&hradio->hax5043);
	if (ret) {
		return ret;
	}
	ret = ax5043_set_pinfuncpwramp(&hradio->hax5043, 0, 0, PWRAMP_OUTPUT_PWRAMP);
	if (ret) {
		return ret;
	}
	ret = ax5043_set_pwramp(&hradio->hax5043, PA_DISABLE);
	if (ret) {
		return ret;
	}
	/* Enable the RF switch */
	ret = ax5043_set_pinfuncantsel(&hradio->hax5043, 0, 0, AX5043_RF_SWITCH_ENABLE);
	if (ret) {
		return ret;
	}
	return NO_ERROR;
}

void
radio_frame_received(const uint8_t *pdu, size_t len)
{
	if (!pdu || !rx_queue || len > MAX_RX_FRAME_LEN) {
		return;
	}
	rx_msg.len = len;
	memcpy(rx_msg.pdu, pdu, len);
	BaseType_t high_prio_woken = pdFALSE;
	xQueueSendFromISR(rx_queue, &rx_msg, &high_prio_woken);
}

/**
 * The TX task is responsible for sending frames to the earth using the
 * AX5043 IC. To do so, it dequeues frames from the \p tx_queue. Tasks that
 * should send data to the ground station shall use the message queue and NOT
 * the AX5043 IC directly.
 */
void
radio_tx_task()
{
	struct radio *hradio = &hqubik.hradio;
	uint8_t wdgid = 0;
	int ret = 1;
	/* Try to register to the watchdog */
	while (ret) {
		ret = watchdog_register(&hwdg, &wdgid);
	}
	while (1) {
		if (xQueueReceive(tx_queue, &tx_msg, pdMS_TO_TICKS(2)) == pdPASS) {
			ax5043_tx_frame(&hradio->hax5043, (const uint8_t *)tx_msg.pdu,
			                tx_msg.len, tx_msg.timeout_ms);
		}
		watchdog_reset_subsystem(&hwdg, wdgid);
	}
}
