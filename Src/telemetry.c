/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  @file telemetry.c
 *
 *  @date Jul 24, 2020
 *  @brief Telemetry data functions
 */

#include <telemetry.h>
#include <string.h>

size_t
telemetry_assemble_regular_frame(struct qubik *q, uint8_t *buffer)
{
	uint8_t status = 0;
	size_t cntr = 0;
	// Voltage
	if (q->status.power_mon_fail) {
		memcpy(buffer + cntr, &q->power.voltage_alt,
		       sizeof(q->power.voltage_alt));
	} else {
		memcpy(buffer + cntr, &q->power.voltage, sizeof(q->power.voltage));
	}
	cntr += sizeof(q->power.voltage);

	// Average current

	memcpy(buffer + cntr, &q->power.current_avg, sizeof(q->power.current_avg));
	cntr += sizeof(q->power.current_avg);

	// State of charge
	memcpy(buffer + cntr, &q->power.SOC, sizeof(q->power.SOC));
	cntr += sizeof(q->power.SOC);

	// Battery temperature
	memcpy(buffer + cntr, &q->power.temperature, sizeof(q->power.temperature));
	cntr += sizeof(q->power.temperature);

	// MCU temperature
	memcpy(buffer + cntr, &q->power.temperature_mcu,
	       sizeof(q->power.temperature_mcu));
	cntr += sizeof(q->power.temperature_mcu);

	// uptime
	memcpy(buffer + cntr, &q->uptime_secs, sizeof(q->uptime_secs));
	cntr += sizeof(q->uptime_secs);

	// Reason of reset
	memcpy(buffer + cntr, &q->uptime_secs, sizeof(q->uptime_secs));
	cntr += sizeof(q->uptime_secs);

	// Dropped frames
	memcpy(buffer + cntr, &q->settings.dropped_frames,
	       sizeof(q->settings.dropped_frames));
	cntr += sizeof(q->settings.dropped_frames);

	// Hardware status
	status = q->status.power_save
	         | q->status.power_save << 1;
	memcpy(buffer + cntr,
	       &status,
	       1);
	cntr ++;
	return cntr;
}

size_t
telemetry_assemble_extended_frame(struct qubik *q, uint8_t *buffer)
{
	size_t cntr = 0;
	return cntr;
}
